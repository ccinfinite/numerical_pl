# CCI-UŚ

***This is the repository for the developement of University of Silesia's part in the CCI project.***

# O1B

Specialised methods for non-IT arts and sciences.

This project will consist of 6 lessons for numerical methods.

* First one discussses errors and uncertainties.
* Second - graphics and matplotlib.
* Third and fourth - numpy library (mainly arrays and array operations)
* Fifth - root finding (equations and systems of linear equations)
* Sixth - Integrals and derivatives
